# How to set up headless Raspberry Pi

## Install Raspberry Pi

1. Download and install [**Raspberry Pi Imager**](https://www.raspberrypi.org/downloads/)

2. Create an empty file without extension in root of the boot image. Name it `ssh`.

3. Copy [network file](wpa_supplicant.conf) to root of boot image and edit SSID and password to match your network.

4. Eject SD-card, insert in Raspberry Pi and boot.

## SSH connect

1. Open terminal and pase: `ssh -o PubkeyAuthentication=no pi@<ip-of-raspberrypi>`

2. Enter the password `raspberry`

3. When connected enter `sudo raspi-config`

4. Set a new name and password of the Raspberry Pi. Enable VNC if you want to.

## Set up terminal

1. Copy contents of [zsh config](.zshrc) and place it in user directory

2. Install autojump: `sudo apt-get install autojump`

3. Install zsh-autosuggestions: `git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`

4. Install zsh-syntax-highlighting: `git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`

5. Reload zsh by entering this in terminal: `. ~/.zshrc`

## Install Node

1. Install NVM by entering this in terminal: `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash`

2. Run `nvm install node`
